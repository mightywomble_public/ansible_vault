---
- hosts: localhost
  become_user: root
  environment:
    VAULT_ADDR: https://127.0.0.1:8200

  vars:
    vaultdata: /opt/vault/data
    vaultip: "0.0.0.0:8200"
    unseal_keys_dir_output: /root/vault/keys
    root_token_dir_output: /root/vault/tokens
    certdir: /opt/vault/tls
    key_size: 4096
    server_hostname: vault
    passphrase: # Set if you want passphras
    key_type: RSA 
    country_name: GB
    email_address: david@davidfield.co.uk
    organization_name: homeoffice

  tasks:
  ##Install Prereqs
    - name: (VAULT - Pre Reqs) install dependencies
      yum:
        name: yum-utils
        state: latest

    - name: (VAULT - Pre Reqs)  pip install ply version 3.8 for hvac[parser]
      pip:
        name: ply
        version: "3.8"
        extra_args: --user

    - name: (VAULT - Pre Reqs) Install python packages
      pip:
        name:
          - hvac
          - hvac[parser]
        #extra_args: --user



  ##Install Hashicorp Vault

    - name: (VAULT - Install) Add repository
      yum_repository:
        name: hashicorp
        description: Hashicorp Repository
        baseurl: https://rpm.releases.hashicorp.com/RHEL/$releasever/$basearch/stable
        gpgkey: https://rpm.releases.hashicorp.com/gpg
  
    - name: (VAULT - Install) install Hashicorp Vault
      yum:
        name: vault
        state: latest


    - name: (VAULT - Install) Create a directory if it does not exist
      ansible.builtin.file:
        path: 
          - "{{ vaultdata }}"
        state: directory
        recurse: yes
        owner: vault
        group: vault
        mode: '0777'

    - name: (VAULT - Install) Remove file vault.hcl (delete file)
      ansible.builtin.file:
        path: /etc/vault.d/vault.hcl
        state: absent

    - name: (VAULT - Install) Copy Config file
      template:
        dest: /etc/vault.d/vault.hcl
        src: config.hcl.j2
        owner: vault
        group: vault
        mode: '0644'

  ##create certificates

    - name: Generate an OpenSSL private key
      openssl_privatekey:
        path: "{{ certdir }}/{{ server_hostname }}.key"
        size: "{{ key_size }}"
        type: "{{ key_type }}"
        backup: yes

    - name: Generate an OpenSSL Certificate Signing Request with Subject information
      openssl_csr:
        path: "{{ certdir }}/{{ server_hostname }}.csr"
        privatekey_path: "{{ certdir }}/{{ server_hostname }}.key"
        country_name: "{{ country_name }}"
        organization_name: "{{ organization_name }}"
        email_address: "{{ email_address }}"
        common_name: "{{ server_hostname }}"

    - name: Generate a Self Signed OpenSSL certificate
      community.crypto.x509_certificate:
        path: "{{ certdir }}/{{ server_hostname }}.crt"
        privatekey_path: "{{ certdir }}/{{ server_hostname }}.key"
        csr_path: "{{ certdir }}/{{ server_hostname }}.csr"
        provider: selfsigned

    
    - name: (VAULT - Install) Set Ownership of keys
      ansible.builtin.file:
        path: '{{ item }}'
        owner: vault
        group: vault
        mode: '0600'
      loop:
        - "{{ certdir }}/{{ server_hostname }}.crt"
        - "{{ certdir }}/{{ server_hostname }}.key"


    ## Start vault  

    - name: (VAULT - Install) Start vault service
      systemd:
        state: restarted
        name: vault
        enabled: yes
        daemon_reload: yes

    - name:  (VAULT - Install) Open port 8200
      ansible.posix.firewalld:
        port: 8200/tcp
        permanent: yes
        state: enabled

    - name: (VAULT - Install) reload service firewalld
      systemd:
        name: firewalld
        state: reloaded


  ## Create Hashicorp Vault keys and token

    - name: (VAULT - Keys) Create unseal directories
      file:
        path: "{{ unseal_keys_dir_output }}"
        state: directory

    - name: (VAULT - Keys) Create root key directories
      file:
        path: "{{ root_token_dir_output }}"
        state: directory

    - name: (VAULT - Keys) Initialise Vault operator
      shell: vault operator init -key-shares=5 -key-threshold=3 -format json
      environment:
        VAULT_SKIP_VERIFY: '1'
        VAULT_ADDR: "https://127.0.0.1:8200"
      register: vault_init_results

    - name: (VAULT - Keys) Parse output of vault init
      set_fact:
        vault_init_parsed: "{{ vault_init_results.stdout | from_json }}"

    - name: (VAULT - Keys) Write unseal keys to files
      copy:
        dest: "{{ unseal_keys_dir_output }}/unseal_key_{{ item.0 }}"
        content: "{{ item.1 }}"
      with_indexed_items: "{{ vault_init_parsed.unseal_keys_hex }}"
     

    - name: (VAULT - Keys) Write root token to file
      copy:
        content: "{{ vault_init_parsed.root_token }}"
        dest: "{{root_token_dir_output}}/rootkey"
    
    - name: (VAULT - Keys) set toot token as fact
      set_fact: 
        vault_token: "{{ vault_init_parsed.root_token }}"
        cacheable: yes

    - debug: msg="{{ vault_token }}"


    - name: (VAULT - Install) Add environmental vars
      blockinfile:
        path: /etc/environment
        block: |
          export VAULT_ADDR='https://127.0.0.1:8200'
          export VAULT_TOKEN="{{ vault_token }}"
          export VAULT_SKIP_VERIFY=1

  ## unseal vault

    - name: Find jar files
      find:
        paths: "{{ unseal_keys_dir_output }}"
        patterns: "unseal_key_*"
      register: unseal_keys
      changed_when: False

    #- name: (VAULT - Unseal) Reading unseal key contents
    #  command: cat {{item}}
    #  register: unseal_keys
    #  with_fileglob: "{{ unseal_keys_dir_output }}/*"
    
    - debug: 
        msg="{{ item.1 }} - "
      with_indexed_items: "{{ vault_init_parsed.unseal_keys_hex }}"

    - name: (VAULT - Unseal) Unseal vault with unseal keys
      shell: |
        vault operator unseal {{ item.1 }}
      environment:
        VAULT_SKIP_VERIFY: '1'
        VAULT_ADDR: "https://127.0.0.1:8200"
      #with_indexed_items: "{{ unseal_keys.files.path }}"
      with_indexed_items: "{{ vault_init_parsed.unseal_keys_hex }}"
   